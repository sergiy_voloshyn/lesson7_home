package com.sergio;

import com.sergio.model.*;
import com.sergio.model.*;
import com.sergio.model.Currency;

import java.util.*;


public class Main {

    static final int organizationCount = 7;


    public static void main(String[] args) {
        // write your code here

        List<Finance> organizations = new ArrayList<>();

        //Finance[] organizations = new Finance[organizationCount];

        Currency[] currenciesExchanger = {new Currency("USD", 27.5F, 27.0F),
                new Currency("EUR", 31.0F, 30.0F)};

        organizations.add(new Exchanger("Обменка 1", "Харьков 1", currenciesExchanger));

        organizations.add(new Lombard("Ломбард 2", "Харьков 2"));
        organizations.add(new CreditCafe("Кредит-кафе 3", "Харьков 3"));
        organizations.add(new CreditAlliance("Кредитный союз 4", "Харьков 4"));
        organizations.add(new PIF("ПИФ 4", "Харьков 4", 2000));
        organizations.add(new Post("Почта 5", "Харьков 5"));

        Currency[] currenciesBank = {new Currency("USD", 27.0F, 26.5F),
                new Currency("EUR", 30.0F, 29.0F),
                new Currency("RUB", 0.45F, 0.46F)};

        organizations.add(new Bank("МегаБанк 6", "Харьков 6", currenciesBank));

//-------------------------------------------------------

        Collections.sort(organizations);
        for (int i = 0; i < organizations.size(); i++) {
            System.out.println(organizations.get(i));
        }

        printBestExchangeRate(organizations, 20_000F, "USD");
        printBestExchangeRate(organizations, 20_000F, "EUR");
        printBestExchangeRate(organizations, 20_000F, "RUB");

        printBestCreditRate(organizations, 50_000);

        printBestDepositRate(organizations, 12, 120);

        printBestSender(organizations, 1000);

        compareTestLinkedList();
        compareTestArrayList();

        /*
        Linked list add elements, ms: 189
        Linked list remove elements, ms: 13
        Array list add elements, ms: 4301
        Array list remove elements, ms: 1753
         */

    }

    //-------------------------------------------------------
    static void compareTestLinkedList() {
        long start = System.currentTimeMillis();

        LinkedList<Finance> list = new LinkedList<>();

        for (int i = 0; i < 100_000; i++) {
            list.add(0, new Finance("Finance" + i, "Kharkiv" + i));
        }

        long add = System.currentTimeMillis() - start;
        System.out.println("Linked list add elements, ms: " + add);

        Iterator<Finance> iterator = list.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getName() != "") {

                iterator.remove();
            }
        }
        long remove = System.currentTimeMillis() - start - add;
        System.out.println("Linked list remove elements, ms: " + remove);

    }

    static void compareTestArrayList() {
        long start = System.currentTimeMillis();

        ArrayList<Finance> list = new ArrayList<>();

        for (int i = 0; i < 100_000; i++) {
            list.add(0, new Finance("Finance" + i, "Kharkiv" + i));
        }

        long add = System.currentTimeMillis() - start;
        System.out.println("Array list add elements, ms: " + add);

        for (int i = 0; i < list.size(); i++) {
            list.remove(i);

        }


        long remove = System.currentTimeMillis() - start - add;
        System.out.println("Array list remove elements, ms: " + remove);

    }


    static void printBestSender(List<Finance> organizations, float money) {


        float minSum = 0;
        Finance bestOrganization = new Finance("", "");

        for (Finance organization : organizations) {

            if (organization instanceof Sender) {

                float sentMoney = ((Sender) organization).sendMoney(money);

                if (minSum == 0) {
                    minSum = sentMoney;
                    bestOrganization = organization;
                    continue;
                }
                if (minSum > 0) {
                    if (sentMoney < minSum) {
                        minSum = sentMoney;
                        bestOrganization = organization;
                    }
                }
            }
        }
        if (!bestOrganization.getName().equalsIgnoreCase("")) {
            System.out.println("\n");

            System.out.println("Organization sender is:" + bestOrganization.getName() + " " + bestOrganization.getAddress());
            System.out.println("Min commission is:" + ((Sender) bestOrganization).getCommissionForSend());
            System.out.println("Min percent is:" + ((Sender) bestOrganization).getPercentForSend());

        }

    }

    static void printBestDepositRate(List<Finance> organizations, int startMonth, int endMonth) {


        float maxPercent = 0;
        Finance bestOrganization = new Finance("", "");

        for (Finance organization : organizations) {

            if (organization instanceof Deposit) {

                boolean yesDeposit = ((Deposit) organization).getDeposit(startMonth, endMonth);

                if ((yesDeposit) & (maxPercent == 0)) {
                    maxPercent = ((Deposit) organization).getDepositPercent();
                    bestOrganization = organization;
                    continue;
                }
                if ((yesDeposit) & (maxPercent > 0)) {
                    if (((Deposit) organization).getDepositPercent() > maxPercent) {
                        maxPercent = ((Deposit) organization).getDepositPercent();
                        bestOrganization = organization;
                    }
                }
            }
        }
        if (!bestOrganization.getName().equalsIgnoreCase("")) {
            System.out.println("\n");
            System.out.println("Percent :" + maxPercent);
            System.out.println("Organization is:" + bestOrganization.getName() + " " + bestOrganization.getAddress());
            System.out.println("Min months is:" + ((Deposit) bestOrganization).getMinMonthCount());
            System.out.println("Max percent is:" + ((Deposit) bestOrganization).getDepositPercent());

        }

    }


    static void printBestCreditRate(List<Finance> organizations, float money) {


        float minPercent = 0;
        Finance bestOrganization = new Finance("", "");

        for (Finance organization : organizations) {

            if (organization instanceof Credit) {

                boolean yesCredit = ((Credit) organization).getCredit(money);

                if ((yesCredit) & (minPercent == 0)) {
                    minPercent = ((Credit) organization).getMAxCreditPercent();
                    bestOrganization = organization;
                    continue;
                }
                if ((yesCredit) & (minPercent > 0)) {
                    if (((Credit) organization).getMAxCreditPercent() < minPercent) {
                        minPercent = ((Credit) organization).getMAxCreditPercent();
                        bestOrganization = organization;
                    }
                }
            }
        }
        if (!bestOrganization.getName().equalsIgnoreCase("")) {
            System.out.println("\n");
            System.out.println("Percent :" + minPercent);
            System.out.println("Organization is:" + bestOrganization.getName() + " " + bestOrganization.getAddress());
            System.out.println("Max to credit is:" + ((Credit) bestOrganization).getMaxCreditSum());
            System.out.println("Min percent is:" + ((Credit) bestOrganization).getMAxCreditPercent());

        }

    }


    static void printBestExchangeRate(List<Finance> organizations, float currency, String currencyName) {


        float maxExcUSD = 0F;
        Finance bestOrganization = new Finance("", "");


        for (Finance organization : organizations) {

            if (organization instanceof Convert) {

                if ((((Convert) organization).sellCurrency(currency, currencyName) > 0) & (maxExcUSD == 0)) {
                    maxExcUSD = ((Convert) organization).sellCurrency(currency, currencyName);
                    bestOrganization = organization;
                    continue;
                }

                if ((((Convert) organization).sellCurrency(currency, currencyName) > maxExcUSD) & (maxExcUSD > 0)) {
                    maxExcUSD = ((Convert) organization).sellCurrency(currency, currencyName);
                    bestOrganization = organization;
                }
            }
        }
        if (!bestOrganization.getName().equalsIgnoreCase("")) {
            System.out.println("\n");
            System.out.println("Currency :" + maxExcUSD + currencyName);
            System.out.println("Organization is:" + bestOrganization.getName() + " " + bestOrganization.getAddress());
            System.out.println("Max sum to convert is:" + ((Convert) bestOrganization).getMaxSumToConvert());
        }

    }


}



