package com.sergio.model;

/**
 * Created by Student on 05.12.2017.
 */
public class Lombard extends Finance implements Credit {


    final float maxSumCreditMoney = 50_000F;
    final float maxCreditPercent = 40;

    public Lombard(String name, String address) {
        super(name, address);
    }

    @Override
    public float getMaxCreditSum() {

        return maxSumCreditMoney;
    }

    @Override
    public float getMAxCreditPercent() {

        return maxCreditPercent;
    }

    @Override
    public boolean getCredit(float sum) {
        return sum <= maxSumCreditMoney;
        //return sum <= maxSumCreditMoney & percent <= maxCreditPercent;
    }
}
