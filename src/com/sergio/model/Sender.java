package com.sergio.model;

/**
 * Created by Student on 05.12.2017.
 */
public interface Sender {

    float getPercentForSend();
    float getCommissionForSend();

    float sendMoney(float money);

}
