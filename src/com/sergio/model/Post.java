package com.sergio.model;

public class Post extends Finance implements Sender {

    final float commissionPercentForSend = 2F;


    public Post(String name, String address) {
        super(name, address);
    }

    @Override
    public float getPercentForSend() {
        return commissionPercentForSend;
    }

    @Override
    public float sendMoney(float money) {

        return money+money / 100 * commissionPercentForSend;
    }

    @Override
    public float getCommissionForSend() {
        return 0;
    }
}
