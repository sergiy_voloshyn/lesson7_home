package com.sergio.model;

/**
 * Created by Student on 05.12.2017.
 */
public class Bank extends Finance implements Convert, Credit, Deposit, Sender {
    Currency[] currencies;

    final float commissionCurrencyExchange = 15F;
    final float maxSumCurrencyExchange = 12_000F;

    final float maxSumCreditMoney = 200_000F;
    final float maxCreditPercent = 25F;

    final float depositPercent = 15;
    final int minMonthsDeposit = 0;
    final int maxMonthsDeposit = 12;

    final float commissionPercentForSend = 1F;
    final float commissionForSend = 5F;


    public Bank(String name, String address, Currency[] currencies) {
        super(name, address);
        this.currencies = currencies;
    }

    @Override
    public float buyCurrency(float currency, String currencyName) {

        if (currency <= maxSumCurrencyExchange) {
            for (int i = 0; i < currencies.length; i++) {
                if (currencyName.equalsIgnoreCase(currencies[i].name)) {

                    return currency * currencies[i].buyCurrencyRate + commissionCurrencyExchange;
                }
            }
        }
        return 0;
    }

    @Override
    public float sellCurrency(float currency, String currencyName) {

        if (currency <= maxSumCurrencyExchange) {

            for (int i = 0; i < currencies.length; i++) {
                if (currencyName.equalsIgnoreCase(currencies[i].name)) {

                    return currency / currencies[i].sellCurrencyRate + commissionCurrencyExchange;
                }
            }
        }
        return 0;
    }

    @Override
    public float getMaxSumToConvert() {
        return maxSumCurrencyExchange;
    }

    @Override
    public float getMaxCreditSum() {
        return maxSumCreditMoney;
    }

    @Override
    public float getMAxCreditPercent() {
        return maxCreditPercent;
    }


    @Override
    public boolean getCredit(float sum) {
        return sum <= maxSumCreditMoney;


    }

    @Override
    public int getMinMonthCount() {
        return minMonthsDeposit;
    }

    @Override
    public int getMaxMonthCount() {
        return maxMonthsDeposit;
    }

    @Override
    public boolean getDeposit(int minMonth, int maxMonth) {


        return minMonth <= minMonthsDeposit & maxMonth <= maxMonthsDeposit;
    }

    @Override
    public float getDepositPercent() {
        return depositPercent;
    }

    @Override
    public float getPercentForSend() {
        return commissionPercentForSend;
    }

    @Override
    public float getCommissionForSend() {
        return commissionForSend;
    }

    @Override
    public float sendMoney(float money) {

        return money+money / 100 * commissionPercentForSend + commissionForSend;

    }

}



