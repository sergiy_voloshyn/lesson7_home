package com.sergio.model;

/**
 * Created by Student on 05.12.2017.
 */
public interface Credit {

    float getMaxCreditSum();

    float getMAxCreditPercent();

    boolean getCredit(float sum);


}
