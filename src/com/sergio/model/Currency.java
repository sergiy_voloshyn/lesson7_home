package com.sergio.model;


public class Currency {
    String name;
    float sellCurrencyRate;
    float buyCurrencyRate;

    public Currency(String name, float sellCurrencyRate, float buyCurrencyRate) {
        this.name = name;
        this.sellCurrencyRate = sellCurrencyRate;
        this.buyCurrencyRate = buyCurrencyRate;
    }

    public float getSellRate(String currency) {
        if (currency.equalsIgnoreCase(this.name)) {
            return sellCurrencyRate;
        } else return 0;

    }

    public float getBuyRate(String currency) {
        if (currency.equalsIgnoreCase(this.name)) {
            return buyCurrencyRate;
        } else return 0;
    }
}


