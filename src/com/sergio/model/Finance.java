package com.sergio.model;

public class Finance implements Comparable<Finance> {
    String name;
    String address;

    public String getName() {
        return this.name;
    }

    public String getAddress() {
        return address;
    }

    public Finance(String name, String address) {
        this.name = name;
        this.address = address;
    }


    @Override
    public int compareTo(Finance finance) {

        return this.name.compareTo(finance.name);

    }

    @Override
    public String toString() {
        super.toString();
        return "Organisation {" +
                "name='" + name + '\'' + ", Address=" + address + '}';
    }
}
