package com.sergio.model;

public class CreditAlliance extends Finance implements Credit {

    final float maxSumCreditMoney = 100_000F;
    final float maxCreditPercent = 20;

    public CreditAlliance(String name, String address) {
        super(name, address);
    }

    @Override
    public float getMaxCreditSum() {
        return maxSumCreditMoney;
    }

    @Override
    public float getMAxCreditPercent() {
        return maxCreditPercent;
    }

    @Override
    public boolean getCredit(float sum) {

        return sum <= maxSumCreditMoney;

    }
}



