package com.sergio.model;

public class CreditCafe extends Finance implements Credit {

    final float maxSumCreditMoney = 4_000F;
    final float maxCreditPercent = 200F;

    public CreditCafe(String name, String address) {
        super(name, address);
    }

    @Override
    public float getMaxCreditSum() {
        return maxSumCreditMoney;
    }

    @Override
    public float getMAxCreditPercent() {
        return maxCreditPercent;
    }

    @Override
    public boolean getCredit(float sum) {
        return sum <= maxSumCreditMoney;
        //return sum <= maxSumCreditMoney & percent <= maxCreditPercent;

    }
}
