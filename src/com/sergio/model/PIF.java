package com.sergio.model;

public class PIF extends Finance implements Deposit {

    int yearOfFoundation;
    final float depositPercent = 22;
    final int minMonthsDeposit = 12;
    final int maxMonthsDeposit = 1200;

    public PIF(String name, String address, int yearOfFoundation) {
        super(name, address);
        this.yearOfFoundation = yearOfFoundation;
    }

    @Override
    public int getMinMonthCount() {
        return minMonthsDeposit;
    }

    @Override
    public int getMaxMonthCount() {
        return maxMonthsDeposit;
    }

    @Override
    public float getDepositPercent() {
        return depositPercent;
    }

    @Override
    public boolean getDeposit(int minMonth, int maxMonth) {

        return minMonth <= minMonthsDeposit & maxMonth <= maxMonthsDeposit;
    }
}
