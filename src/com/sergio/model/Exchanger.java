package com.sergio.model;

/**
 * Created by Student on 05.12.2017.
 */


public class Exchanger extends Finance implements Convert {

    Currency[] currencies;

    public Exchanger(String name, String address, Currency[] currencies) {
        super(name, address);
        this.currencies = currencies;
    }

    @Override
    public float buyCurrency(float currency, String currencyName) {

        for (int i = 0; i < currencies.length; i++) {
            if (currencyName.equalsIgnoreCase(currencies[i].name)) {

                return currency * currencies[i].buyCurrencyRate;
            }
        }
        return 0;
    }

    @Override
    public float sellCurrency(float currency, String currencyName) {

        for (int i = 0; i < currencies.length; i++) {
            if (currencyName.equalsIgnoreCase(currencies[i].name)) {

                return currency / currencies[i].sellCurrencyRate;
            }
        }
        return 0;
    }

    @Override
    public float getMaxSumToConvert() {
        return 0;
    }
}


